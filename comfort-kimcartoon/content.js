let get = document.getElementById.bind(document);
let $ = document.querySelector.bind(document);
let click = (e) => e?(e.click(),true):false;
let getTopOffset = (e) => e.getBoundingClientRect().top + window.scrollY;
let calculateCenteredScrollPos = (e) => getTopOffset(e) - (document.documentElement.clientHeight - e.clientHeight) / 2;
let scroll = (y) => window.scrollTo(window.scrollX, y);
let scrollToScreenCenter = (e) => e ? (scroll(calculateCenteredScrollPos(e)), true) : false;

chrome.storage.sync.get(
	{
		'autoplay': true,
		'expand': true,
		'switch': true,
		'next': true,
		'scrollOnExpand': true,
		'rememberPos': true
	},
	(settings) => {
		if (settings.scrollOnExpand) {
			let [expandButton, collapseButton, video] = [get('expand'), get('collapse'), get('my_video_1')]
			if (expandButton && collapseButton && video) {
				expandButton.addEventListener('click', () => scrollToScreenCenter(video));
				collapseButton.addEventListener('click', () => scrollToScreenCenter(video));
			}
		}
		if (settings.expand) click(get('expand'));
		if (settings.switch) click(get('switch'));
		if (settings.rememberPos) {
			let video = get('my_video_1_html5_api');
			if (video) {
				let key = `rememberedPos.${window.location.pathname}`;
				chrome.storage.sync.get({[key]: 0}, (data) => {
					if (data[key])
						video.currentTime = data[key];
					setInterval(() => chrome.storage.sync.set({[key]: video.currentTime}), 2000);
				});
			}
		}
		if (settings.autoplay) click($('#my_video_1>button.vjs-big-play-button'));
		if (settings.next) {
			let [next_button, video] = [get('Img2'), get('my_video_1_html5_api')];
			if (next_button && video) video.onended = next_button.click.bind(next_button);
		}
	}
);