let get = document.getElementById.bind(document);

let save_options = () => {
	chrome.storage.sync.set({
		'autoplay': get('autoplay').checked,
		'expand': get('expand').checked,
		'switch': get('switch').checked,
		'next': get('next').checked,
		'scrollOnExpand': get('scrollOnExpand').checked,
		'rememberPos': get('rememberPos').checked
	},
	() => {
		let status = get('status');
		status.textContent = 'Options saved.';
		setTimeout(() => {
		  status.textContent = '';
		}, 750);
	});
}

let restore_options = () => {
	chrome.storage.sync.get({
		'autoplay': true,
		'expand': true,
		'switch': true,
		'next': true,
		'scrollOnExpand': true,
		'rememberPos': true
	},
	(items) => {
		get('autoplay').checked = items['autoplay'];
		get('expand').checked = items['expand'];
		get('switch').checked = items['switch'];
		get('next').checked = items['next'];
		get('scrollOnExpand').checked = items['scrollOnExpand'];
		get('rememberPos').checked = items['rememberPos'];
	});
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);